﻿using UnityEngine;
using System.Collections;

public class Poder : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	    if (Input.GetMouseButtonDown (0)) {
       
        Vector2 pos = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        RaycastHit2D hitInfo = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(pos), Vector2.zero);
        // RaycastHit2D can be either true or null, but has an implicit conversion to bool, so we can use it like this
        if(hitInfo)
        {
        	if(hitInfo.transform.gameObject.tag == ("Poder"))
            Debug.Log( hitInfo.transform.gameObject.name );
            // Here you can check hitInfo to see which collider has been hit, and act appropriately.
        	else if(hitInfo.transform.gameObject.tag == ("Enemy"))
        		hitInfo.transform.gameObject.SetActive(false);
        }
    }
	}
}
